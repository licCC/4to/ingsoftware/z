# Notas

- Los documentos extensión zed se pueden ver en Eclipse y pueden contener más imformación de la que se ve en los pdf.

- Los pdf no tinen ninguna explicación o justificación sobre el tipo de esquema estado elegido. Para la entrega de un ejercicio se debería agregar esto y posiblemente las designaciones correspondientes (al estilo MJ).

## Guía / Orden que puede ayudar a resolver ejercicios

1. Definir funciones o constantes axiomaticas, tipos base, mensajes de error
2. Definir estado -> prioridad a funciones parciales
3. Definir estado inicial (y estados iniciales para esquemas de menor nivel)
4. Definir todas las operaciones con errores -> priorizar la promoción de operaciones; escribir todas las variables de estado en los delta
5. Si piden invariantes:
  - En la especificación: esquema Inv que incluye el estado del sistema
  - Esqueleto que se plantea (no se demuestra). Obligación de prueba:
> Inv \land Op \implies Inv' (para toda Op)

> Init \land Inv
6. Si piden designaciones:
  - Tipos básicos (ej: DINERO, NUMCTA, NOMBRE)
  - Nombres de esquemas de operaciones totales (ej: Extraer)
  - Nombres de esquemas de estado (ej: CajaAhorros, Cliente)
  - Nombres de constantes / funciones axiomáticas (subespecificación) (ej: limiteExtraerCA, encriptarPass)
  - Nombres de constructores de tipos libres (ej: ok, saldoInsuficiente)