\documentclass{article}
\usepackage{czt}

\begin{document}
\begin{zsection}
  \SECTION SistemaDeControlDeVersiones \parents standard\_toolkit
\end{zsection}

This specification describes un sistema de control de versiones

\begin{zed}
  [PNAME, PHEADER, PBODY]
\end{zed}

\begin{axdef}
  nombre_vacio: PNAME
\end{axdef}

\begin{schema}{Programa}
  encabezado: PHEADER\\
  cuerpo: PBODY
\end{schema}

\begin{zed}
  [USERID]
\end{zed}

\begin{zed}
  ROLE ::= AUTOR | EDITOR | LECTOR
\end{zed}

\begin{schema}{CVS}
  uAutorizados: USERID \pfun ROLE  \\
  %Si los permisos estuvieran por programa: uAutorizados: PNAME \pfun USERID \pfun PERMISOS
  %Se puede hacer uAutorizados\ a?\ u? y devuelve los permisos
  %O disenar una función axiomática que te devuelva todos los permisos
  aEditados: PNAME \pfun USERID \\
  lineaBase: PNAME \pfun \seq Programa
\end{schema}

\begin{schema}{InitCVS}
  CVS
\where
  uAutorizados \neq \emptyset  \\
  aEditados = \emptyset \\
  lineaBase = \emptyset
\end{schema}

\begin{zed}
  REPLY ::= ok | noHayCambios | usuarioNoExiste | usuarioNoAutorizado \\
  | archivoExistente | nombreVacioOInvalido | archivoInexistente \\
  | archivoBloqueado | archivoNoBloqueado | archivoBloqueadoPorOtroUsuario
\end{zed}

\section{Modificar encabezado y cuerpo de un programa}

\begin{schema}{PModificarEncabezadoYCuerpo}
  \Delta Programa \\
  h?: PHEADER \\
  b?: PBODY \\
  rep!: REPLY
\where
  h? \neq encabezado \land b? \neq cuerpo\\
  
  encabezado' = h? \\
  cuerpo' = b? \\
  
  rep! = ok
\end{schema}


\begin{schema}{PModificarEncabezadoYCuerpoSinCambios}
  \Xi Programa \\
  h?: PHEADER \\
  b?: PBODY \\
  rep!: REPLY
\where
  \lnot (h? \neq encabezado \land b? \neq cuerpo)\\
  
  rep! = noHayCambios
\end{schema}

\section{Operaciones}

\subsection{Create}

\begin{schema}{CrearPrograma}
  \Delta Programa \\
  h?: PHEADER \\
  b?: PBODY \\
\where
  encabezado' = h? \\
  cuerpo' = b? \\
\end{schema}

\begin{schema}{CreateOk}
  \Delta CVS \\
  CrearPrograma \\
  usr?: USERID \\
  n?: PNAME \\
  rep!: REPLY
\where
  (usr?, AUTOR) \in uAutorizados \\
  n? \notin \dom lineaBase \\
  n? \neq nombre_vacio \\
  
  uAutorizados' = uAutorizados\\
  aEditados' = aEditados\\
  lineaBase' = lineaBase \cup \{n? \mapsto \langle \theta Programa\ ' \rangle\}\\
  
  rep! = ok
\end{schema}

Sin promoción juntando los esquemas. La actualización de lineaBase puede ser
\Exists p:PROGRAM | p.encabezado = h?; p.cuerpo = b? @ lineaBase' = lineaBase \cup \{n? \mapsto p\})

\begin{schema}{UsuarioInexistente}
  \Xi CVS \\
  usr?: USERID \\
  rep!: REPLY
\where
  usr? \notin \dom uAutorizados \\
  
  rep! = usuarioNoExiste
\end{schema}

\begin{schema}{UsuarioNoAutorizadoAutor}
  \Xi CVS \\
  usr?: USERID \\
  rep!: REPLY
\where
  usr? \in \dom uAutorizados \\
  (usr?, AUTOR) \notin uAutorizados \\
  
  rep! = usuarioNoAutorizado
\end{schema}

\begin{schema}{ArchivoExistente}
  \Xi CVS \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \in \dom lineaBase \\
  
  rep! = archivoExistente
\end{schema}

\begin{schema}{NombreVacioOInvalido}
  \Xi CVS \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \notin PNAME \lor n? = nombre_vacio\\
  
  rep! = nombreVacioOInvalido
\end{schema}

\begin{zed}
  CreateE == UsuarioInexistente \lor UsuarioNoAutorizadoAutor \lor ArchivoExistente \lor NombreVacioOInvalido \also
  
  Create == CreateOk \lor CreateE
\end{zed}

\subsection{Get}

\begin{schema}{GetOk}
  \Xi CVS \\
  usr?: USERID \\
  n?: PNAME \\
  prog!: Programa \\
  rep!: REPLY
\where
  %Cualquier lector puede acceder a cualquier archivo
  usr? \in \dom uAutorizados \\
  n? \in \dom lineaBase \\
  
  prog! = last\ (lineaBase\ n?) \\
  
  rep! = ok
\end{schema}

\begin{schema}{ArchivoInexistente}
  \Xi CVS \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \notin \dom lineaBase \\
  
  rep! = archivoInexistente
\end{schema}

\begin{zed}
  GetE == UsuarioInexistente \lor ArchivoInexistente \also
  
  Get == GetOk \lor GetE
\end{zed}

\subsection{Edit}

\begin{schema}{EditOk}
  \Delta CVS \\
  usr?: USERID \\
  n?: PNAME \\
  rep!: REPLY
\where
  (usr?, AUTOR) \in uAutorizados \lor (usr?, EDITOR) \in uAutorizados \\
  n? \in \dom lineaBase \\
  n? \notin \dom aEditados \\
  
  uAutorizados' = uAutorizados\\
  aEditados' = aEditados \cup \{n? \mapsto usr?\}\\
  lineaBase' = lineaBase \\
  
  rep! = ok
\end{schema}

\begin{schema}{UsuarioNoAutorizadoEditor}
  \Xi CVS \\
  usr?: USERID \\
  rep!: REPLY
\where
  usr? \in \dom uAutorizados \\
  (usr?, AUTOR) \notin uAutorizados \\
  (usr?, EDITOR) \notin uAutorizados \\
  
  rep! = usuarioNoAutorizado
\end{schema}

\begin{schema}{ArchivoBloqueado}
  \Xi CVS \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \in \dom aEditados \\
  
  rep! = archivoBloqueado
\end{schema}

\begin{zed}
  EditE == UsuarioInexistente \lor UsuarioNoAutorizadoAutor \lor UsuarioNoAutorizadoEditor \\
  \lor ArchivoInexistente \lor ArchivoBloqueado \also
  
  Edit == EditOk \lor EditE
\end{zed}

\subsection{Delta}

\begin{schema}{DeltaOk}
  \Delta CVS \\
  PModificarEncabezadoYCuerpo \\
  usr?: USERID \\
  n?: PNAME \\
  rep!: REPLY
\where
  (usr?, AUTOR) \in uAutorizados \lor (usr?, EDITOR) \in uAutorizados \\
  n? \in \dom lineaBase \\
  (n?, usr?) \in aEditados \\
  
  uAutorizados' = uAutorizados\\
  aEditados' = aEditados \setminus \{n? \mapsto usr?\}\\
  last (lineaBase\ n?) = \theta Programa \\
  lineaBase' = lineaBase \oplus \{n? \mapsto lineaBase\ n? \cat \langle \theta Programa\ ' \rangle\}\\
  
  rep! = ok
\end{schema}

\begin{schema}{ArchivoNoBloqueado}
  \Xi CVS \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \notin \dom aEditados \\
  
  rep! = archivoNoBloqueado
\end{schema}

\begin{schema}{ArchivoBloqueadoPorDiferenteUsuario}
  \Xi CVS \\
  usr?: USERID \\
  n?: PNAME \\
  rep!: REPLY
\where
  n? \in \dom aEditados \\
  aEditados\ n? \neq usr? \\
  
  rep! = archivoBloqueadoPorOtroUsuario
\end{schema}

\begin{zed}
  DeltaE == UsuarioInexistente \lor UsuarioNoAutorizadoAutor \lor UsuarioNoAutorizadoEditor\\
  \lor ArchivoInexistente \lor ArchivoNoBloqueado \lor PModificarEncabezadoYCuerpoSinCambios \\
  \lor ArchivoBloqueadoPorDiferenteUsuario \also
  
  Delta == DeltaOk \lor DeltaE
\end{zed}

\subsection{Delete}

\begin{schema}{DeleteOk}
  \Delta CVS \\
  usr?: USERID \\
  n?: PNAME \\
  rep!: REPLY
\where
  (usr?, AUTOR) \in uAutorizados \\
  n? \in \dom lineaBase \\
  n? \notin \dom aEditados \\
  
  uAutorizados' = uAutorizados\\
  aEditados' = aEditados \\
  lineaBase' = \{n?\} \ndres lineaBase\\
  
  rep! = ok
\end{schema}

\begin{zed}
  DeleteE == UsuarioNoAutorizadoAutor \lor ArchivoInexistente \lor UsuarioInexistente \\
  \lor ArchivoBloqueado \also
  
  Delete == DeleteOk \lor DeleteE
\end{zed}

\end{document}